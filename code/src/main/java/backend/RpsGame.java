package backend;

import java.util.Random;

public class RpsGame {
    // TODO
    private int win;
    private int ties;
    private int losses;

    public int getWin() {
        return this.win;
    }

    public void setWin(int win) {
        this.win = win;
    }

    public int getTies() {
        return this.ties;
    }

    public void setTies(int ties) {
        this.ties = ties;
    }

    public int getLosses() {
        return this.losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public RpsGame() {
        this.win = 0;
        this.ties = 0;
        this.losses = 0;
    }

    public String playRound(String choice) {

        Random random = new Random();

        String computerChoice = "";
        String winner = "";

        switch (random.nextInt(3)) {
            case 0:
                computerChoice = "rock";
                if (choice.equals("rock")) {
                    this.ties += 1;
                    winner = "Its a tie";
                    break;
                } else if (choice.equals("scissors")) {
                    this.losses += 1;
                    winner = "the computer won";
                    break;
                } else if (choice.equals("paper")) {
                    this.win += 1;
                    winner = "you won";
                    break;
                }
            case 1:
                computerChoice = "scissors";
                if (choice.equals("paper")) {
                    this.losses += 1;
                    winner = "the computer won";
                    break;
                } else if (choice.equals("rock")) {
                    this.win += 1;
                    winner = "you won";
                    break;
                } else if (choice.equals("scissors")) {
                    this.ties += 1;
                    winner = "Its a tie";
                    break;
                }
            case 2:
                computerChoice = "paper";
                if (choice.equals("scissors")) {
                    this.win += 1;
                    winner = "you won";
                    break;
                } else if (choice.equals("paper")) {
                    this.ties += 1;
                    winner = "Its a tie";
                    break;
                } else if (choice.equals("rock")) {
                    this.losses += 1;
                    winner = "the computer won";
                    break;
                }

        }
        return "Computer plays " + computerChoice + " and " + winner;
    }
}
