package rpsgame;

import java.util.Scanner;

import backend.RpsGame;

/**
 * Console application for rock paper scissors game
 *
 */
public class App {
    private static Scanner scan = new Scanner(System.in);
    private static RpsGame game = new RpsGame();

    private static String validatePlayersChoice(String choice) {
        while (true) {
            switch (choice.toLowerCase()) {
                case "rock":
                    return choice;
                case "scissors":
                    return choice;
                case "paper":
                    return choice;
                default:
                    System.out.println("please enter a valid action choice");
                    choice = scan.nextLine();
            }
        }
    }

    private static boolean getYesOrNo() {
        System.out.println("do you wish to play again");
        while (true) {
            switch (scan.nextLine().toLowerCase()) {
                case "yes":
                    return true;
                case "no":
                    return false;
                default:
                    System.out.println("please enter yes or no");
                    continue;
            }
        }
    }

    public static void main(String[] args) {
        while (true) {
            System.out.println("what do you wish to play?");
            String action = validatePlayersChoice(scan.nextLine());
            System.out.println(game.playRound(action));

            System.out.println("\n\nyour new scores :");
            System.out.println(game.getWin() + " wins");
            System.out.println(game.getTies() + " ties");
            System.out.println(game.getLosses() + " losses");

            if (!getYesOrNo()) {
                break;
            }
        }
    }
}
